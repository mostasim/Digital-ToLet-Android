package tech.mbsoft.digital_to_let.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.util.Util;

public class AdvertiseAdapter extends RecyclerView.Adapter<AdvertiseAdapter.AdvertiseViewHolder>{

    Context context;
    List<AdvertItem> advertItemList;

    public AdvertiseAdapter(Context context, List<AdvertItem> advertItemList) {
        this.context = context;
        this.advertItemList = advertItemList;
    }

    public void setAdvertItemList(List<AdvertItem> advertItemList) {
        this.advertItemList = advertItemList;
        notifyDataSetChanged();
        Log.e("ADAPTER:","Dataset Change");
    }

    @NonNull
    @Override
    public AdvertiseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_advertise_item,parent,false);//set layout here
        return new AdvertiseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdvertiseViewHolder holder, int position) {

        holder.tvTitle.setText(String.valueOf(advertItemList.get(position).getAdvertiseType().charAt(0)));
        holder.tvRoadName.setText(advertItemList.get(position).getRoadName());
        holder.tvPoliceStation.setText(advertItemList.get(position).getPoliceStation());
        holder.tvDistrict.setText(advertItemList.get(position).getDistrict());
        String dateStartFrom=""+advertItemList.get(position).getStartFrom().getDate()+" "+ Util.theMonth(advertItemList.get(position).getStartFrom().getMonth())+", "+advertItemList.get(position).getStartFrom().getYear();
        holder.tvStartDate.setText(dateStartFrom);

    }

    @Override
    public int getItemCount() {
        return advertItemList.size();
    }

    public class AdvertiseViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle,tvRoadName,tvPoliceStation,tvDistrict,tvStartDate;

        public AdvertiseViewHolder(View itemView) {
            super(itemView);

            tvTitle=itemView.findViewById(R.id.tvSingleViewHeadTitle);
            tvRoadName=itemView.findViewById(R.id.tvSingleViewRoadName);
            tvPoliceStation=itemView.findViewById(R.id.tvSingleViewPoliceStation);
            tvDistrict=itemView.findViewById(R.id.tvSingleViewDistrict);
            tvStartDate=itemView.findViewById(R.id.tvSingleViewStartDate);
        }
    }
}
