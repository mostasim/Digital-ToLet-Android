package tech.mbsoft.digital_to_let.database;

import tech.mbsoft.digital_to_let.model.User;

public class DataHelper {

    private static DataHelper instance;
    private User user;

    private DataHelper()
    {

    }
    public static DataHelper getInstance()
    {
        if (instance==null)
        {
            instance=new DataHelper();
        }
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
