package tech.mbsoft.digital_to_let.database;

import android.util.Log;
import android.util.Pair;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.model.User;
import tech.mbsoft.digital_to_let.util.DefaultValue;

public class FireDataBaseHelper  {
    private static final String TAG = "FireDataBaseHelper: ";
    public static FirebaseAuth mAuth=FirebaseAuth.getInstance();

    public static FirebaseUser firebaseUser = mAuth.getCurrentUser();

    // Write  to the database
    public static FirebaseDatabase database = FirebaseDatabase.getInstance();
    public static DatabaseReference userDatabaseRef = database.getReference("user");
    public static DatabaseReference advertDatabaseRef = database.getReference("advertise");
    public static DatabaseReference renttypeDatabaseRef = database.getReference("renttype");

    //Storage
    public static FirebaseStorage storage=FirebaseStorage.getInstance();
    public static StorageReference storageReference=storage.getReference();

    public static boolean addNewAdvertise(AdvertItem advertItem)
    {
        try {
            advertItem.setPost_by_id(firebaseUser.getUid());
            advertDatabaseRef.child(advertDatabaseRef.push().getKey()).setValue(advertItem);
            return true;
        }catch (Exception e)
        {
            Log.d(TAG, "addNewAdvertise: "+e);
            return false;
        }
    }
    public static boolean editAdvertiseItem(AdvertItem advertItem)
    {
        try {
            advertItem.setPost_by_id(firebaseUser.getUid());
            advertDatabaseRef.child(advertItem.getId()).setValue(advertItem);
            return true;
        }catch (Exception e)
        {
            Log.d(TAG, "editAdvertiseItem: "+e);
            return false;
        }
    }
    public static boolean deleteAdvertiseItem(String childID)
    {
        try {
            advertDatabaseRef.child(childID).removeValue();
            return true;
        }catch (Exception e)
        {
            Log.d(TAG, "deleteAdvertiseItem: "+e);
            return false;
        }
    }

    public static boolean createUserDetails(User user)
    {
        if (user!=null && userDatabaseRef !=null && firebaseUser!=null)
        {
            userDatabaseRef.child(user.getId()).setValue(user);
            return true;
        }else
        {
            return false;
        }
    }
    public static User getCurrenUserDetails()
    {
        final User[] user = new User[1];
        ValueEventListener userEventListener=new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user[0] =dataSnapshot.child("user").child(firebaseUser.getUid()).getValue(User.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        userDatabaseRef.addValueEventListener(userEventListener);
        //User user=userDatabaseRef.child(firebaseUser.getUid());
        return user[0];
    }

    public static void setUserEventListener(ValueEventListener eventListener)
    {
        userDatabaseRef.addListenerForSingleValueEvent(eventListener);
    }

    public static void setAdvertiseEventListener(ValueEventListener eventListener, Pair<String, String> pair)
    {
        if (pair.first!=null)
        {
            Query query=advertDatabaseRef.orderByChild(pair.first).equalTo(pair.second);
            query.addValueEventListener(eventListener);
        }else {
            Query query=advertDatabaseRef.orderByKey();
            query.addValueEventListener(eventListener);
        }
    }

    public static FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }
}
