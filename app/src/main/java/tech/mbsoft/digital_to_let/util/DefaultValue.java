package tech.mbsoft.digital_to_let.util;

/**
 * Created by mbSoft on 01-Mar-18.
 */

public class DefaultValue {
    public static final int RC_SIGN_IN = 9001;


    public static final String PairFirst="PAIR_FIRST";
    public static final String PairSecond="PAIR_SECOND";

    public static final String ORDER_BY_RENT_TYPE="advertiseType";
    public static final String ORDER_BY_DISTRICT="district";
    public static final String ORDER_BY_PS="policeStation";
    public static final String ORDER_BY_DATE="post_on";
}
