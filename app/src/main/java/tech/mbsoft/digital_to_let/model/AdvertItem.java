package tech.mbsoft.digital_to_let.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mbSoft on 20-Mar-18.
 */

public class AdvertItem implements Serializable{

    private String id;
    private String post_by_name;
    private String post_by_id;
    private String advertiseType; //ShortView
    private Location location;
    private String price;
    private String mobile;
    private String details;
    private String district; //ShortView
    private String roadName; //ShortView
    private String policeStation; //ShortView
    private Date startFrom; //ShortView
    private Date endFrom;
    private Date post_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_by_name() {
        return post_by_name;
    }

    public void setPost_by_name(String post_by_name) {
        this.post_by_name = post_by_name;
    }

    public String getPost_by_id() {
        return post_by_id;
    }

    public void setPost_by_id(String post_by_id) {
        this.post_by_id = post_by_id;
    }

    public String getAdvertiseType() {
        return advertiseType;
    }

    public void setAdvertiseType(String advertiseType) {
        this.advertiseType = advertiseType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public Date getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(Date startFrom) {
        this.startFrom = startFrom;
    }

    public Date getEndFrom() {
        return endFrom;
    }

    public void setEndFrom(Date endFrom) {
        this.endFrom = endFrom;
    }

    public Date getPost_on() {
        return post_on;
    }

    public void setPost_on(Date post_on) {
        this.post_on = post_on;
    }
}
