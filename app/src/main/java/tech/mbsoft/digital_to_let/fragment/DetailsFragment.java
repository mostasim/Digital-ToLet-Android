package tech.mbsoft.digital_to_let.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.activity.AdvertiseDetailActivity;
import tech.mbsoft.digital_to_let.adapter.AdvertiseAdapter;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.util.DefaultValue;
import tech.mbsoft.digital_to_let.util.RecyclerTouchListener;

import com.google.android.gms.plus.PlusOneButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;


public class DetailsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    Pair<String,String> pair;

    private ArrayList<AdvertItem> advertItemList;
    AdvertiseAdapter advertiseAdapter;
    RecyclerView recyclerView;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(DefaultValue.PairFirst);
            mParam2 = getArguments().getString(DefaultValue.PairSecond);
            pair=new Pair<>(mParam1,mParam2);
        }
        advertItemList=new ArrayList<>();

        ValueEventListener eventListener=new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                advertItemList.clear(); //very very important to notify data set
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                {
                    AdvertItem item=dataSnapshot1.getValue(AdvertItem.class);
                    item.setId(dataSnapshot1.getKey());
                    advertItemList.add(item);

                }
                advertiseAdapter.setAdvertItemList(advertItemList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        FireDataBaseHelper.setAdvertiseEventListener(eventListener,pair);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        recyclerView=view.findViewById(R.id.rvAdvertList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        advertiseAdapter=new AdvertiseAdapter(getContext(),advertItemList);
        recyclerView.setAdapter(advertiseAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AdvertItem advertItem=advertItemList.get(position);
                Intent intent=new Intent(getContext(), AdvertiseDetailActivity.class);
                intent.putExtra("ITEM",advertItem);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, final int position) {

                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("Won't be able to recover this file!")
                        .setCancelText("Cancel")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        })
                        .setConfirmText("Delete")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                if (FireDataBaseHelper.deleteAdvertiseItem(advertItemList.get(position).getId()))
                                {
                                    Toast.makeText(getContext(), "Advertise deleted ...", Toast.LENGTH_SHORT).show();
                                }else
                                {
                                    Toast.makeText(getContext(), "Deletion Failed!", Toast.LENGTH_SHORT).show();
                                }
                                sDialog
                                        .setTitleText("Deleted")
                                        .setContentText("The selected advertise post is deleted")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .show();

                //Toast.makeText(getContext(), "Long Click"+position, Toast.LENGTH_SHORT).show();

            }
        }));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



}
