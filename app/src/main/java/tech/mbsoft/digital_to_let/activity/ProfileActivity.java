package tech.mbsoft.digital_to_let.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.model.User;
import tech.mbsoft.digital_to_let.util.Util;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int PICK_IMAGE_REQUEST = 1;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private Uri filePath;

    TextView tvName,tvEmail,tvRegDate,tvBirthDate;
    Button btEditProfile, btAddPostInProfile,btEditPhoto;
    ImageView ivProfilePhoto;
    User[] user1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setHomeButtonEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        initialViews();
        user1 = new User[1];
        updateUI();



    }

    private void updateUI() {
        ValueEventListener valueEventListener=new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                user1[0]=dataSnapshot.child(user.getUid()).getValue(User.class);
                user1[0].setEmail(user.getEmail());

                String name=  dataSnapshot.child(user.getUid()).child("name").getValue(String.class);
                Date regDate=dataSnapshot.child(user.getUid()).child("registrationDate").getValue(Date.class);
                Date birthDate=dataSnapshot.child(user.getUid()).child("birthDate").getValue(Date.class);

                tvName.setText("Name: "+name );
                tvBirthDate.setText("Birth Date: "+Util.getDateFormate(birthDate));
                tvRegDate.setText("Registraion Date: "+Util.getDateFormate(regDate));
                Log.e("CAST USER:",user.getUid());
                if (user1[0].getProfileImageURL()!=null)
                    updateProfileImage();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        FireDataBaseHelper.setUserEventListener(valueEventListener);

        tvEmail.setText("Email: "+user.getEmail());
    }

    private void updateProfileImage() {
        StorageReference storageReference =FireDataBaseHelper.storageReference.child(user1[0].getProfileImageURL());
        //Circle Image
        //Glide.with(context).load(url).apply(RequestOptions.circleCropTransform()).into(imageView);
        Glide.with(this )
                .load(storageReference)
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfilePhoto);
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final String uploadPath="images/"+UUID.randomUUID().toString()+"."+Util.getMimeType(this,filePath);  // TODO: 11-Apr-18 File path set to download from server
            StorageReference ref = FireDataBaseHelper.storageReference.child(uploadPath);

            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                            user1[0].setProfileImageURL(uploadPath);
                            FireDataBaseHelper.createUserDetails(user1[0]);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }
    private void initialViews() {

        tvName=findViewById(R.id.tvName);
        tvEmail=findViewById(R.id.tvEmail);
        tvRegDate=findViewById(R.id.tvRegistrationDate);
        tvBirthDate=findViewById(R.id.tvBirthDate);

        btEditProfile=findViewById(R.id.btEditProfile);
        btAddPostInProfile =findViewById(R.id.btAddPostInProfile);
        btEditPhoto=findViewById(R.id.btEditPhoto);

        ivProfilePhoto=findViewById(R.id.ivProfilePhoto);

        btEditProfile.setOnClickListener(this);
        btAddPostInProfile.setOnClickListener(this);
        btEditPhoto.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(this )
                        .load(bitmap)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivProfilePhoto);
                //ivProfilePhoto.setImageBitmap(bitmap);
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (requestCode==100 && resultCode==RESULT_OK)
        {
            updateUI();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btEditProfile:
                    startActivityForResult(new Intent(ProfileActivity.this,EditProfileActivity.class),100);
                break;
            case R.id.btAddPostInProfile:
                    startActivity(new Intent(ProfileActivity.this,AddAdvertisePost.class));
                break;
            case R.id.btEditPhoto:
                chooseImage();
                break;
        }
    }


}
