package tech.mbsoft.digital_to_let.activity;


import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.CharacterPickerDialog;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.DataHelper;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.fragment.DetailsFragment;
import tech.mbsoft.digital_to_let.model.User;
import tech.mbsoft.digital_to_let.util.DefaultValue;

public class DashBoard extends AppCompatActivity implements Drawer.OnDrawerItemClickListener{

    Drawer result;
    FloatingActionButton fabAddAdvertItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);


        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Dashboard");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fabAddAdvertItem=findViewById(R.id.fabAddAdvertItem);

        //Viewpager and Tab layout setup
        Bundle bundle= new Bundle();
        bundle.putString(DefaultValue.PairFirst,"advertiseType");
        bundle.putString(DefaultValue.PairSecond,"Family");
        Bundle bundle2= new Bundle();
        bundle2.putString(DefaultValue.PairFirst,"advertiseType");
        bundle2.putString(DefaultValue.PairSecond,"Single Seat");
        Bundle bundle3= new Bundle();
        bundle3.putString(DefaultValue.PairFirst,"advertiseType");
        bundle3.putString(DefaultValue.PairSecond,"Mess");
        Bundle bundle4= new Bundle();
        bundle4.putString(DefaultValue.PairFirst,"advertiseType");
        bundle4.putString(DefaultValue.PairSecond,"Office");
        Bundle bundle5= new Bundle();
        bundle5.putString(DefaultValue.PairFirst,"advertiseType");
        bundle5.putString(DefaultValue.PairSecond,"ATM");
        Bundle bundle6= new Bundle();
        bundle6.putString(DefaultValue.PairFirst,"advertiseType");
        bundle6.putString(DefaultValue.PairSecond,"Room");
        Bundle bundle7= new Bundle();
        bundle7.putString(DefaultValue.PairFirst,"advertiseType");
        bundle7.putString(DefaultValue.PairSecond,"Shop");

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("All", DetailsFragment.class)
                .add("Family",DetailsFragment.class ,bundle)
                .add("Seat",DetailsFragment.class ,bundle2)
                .add("Mess",DetailsFragment.class ,bundle3)
                .add("Office",DetailsFragment.class ,bundle4)
                .add("ATM",DetailsFragment.class ,bundle5)
                .add("Room",DetailsFragment.class ,bundle6)
                .add("Shop",DetailsFragment.class ,bundle7)
                .create());

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);



        fabAddAdvertItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashBoard.this,AddAdvertisePost.class));
                //Toast.makeText(DashBoard.this, "FAB "+viewPager.getCurrentItem(), Toast.LENGTH_SHORT).show();
            }
        });


        // Create the AccountHeader
        User currenUser= DataHelper.getInstance().getUser();

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        (currenUser!=null)?
                            new ProfileDrawerItem().withName(""+currenUser.getName()).withEmail(""+FireDataBaseHelper.getFirebaseUser().getEmail()).withIcon(getResources().getDrawable(R.drawable.profile2))
                                :new ProfileDrawerItem().withName("User")
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }

                })
                .build();

        //Drawer Layout setup
        result= new DrawerBuilder(this).
                withRootView(R.id.drawer_container).
                withToolbar(toolbar).
                withDisplayBelowStatusBar(false).
                withActionBarDrawerToggleAnimated(true).
                withAccountHeader(headerResult).
                addDrawerItems(new PrimaryDrawerItem().withIdentifier(1).withName("Profile").withIcon(R.drawable.ic_person_black_24dp),
                        //new PrimaryDrawerItem().withIdentifier(2).withName("Setting").withIcon(R.drawable.ic_settings_black_24dp),
                        new PrimaryDrawerItem().withIdentifier(3).withName("About").withIcon(R.drawable.ic_info_black_24dp),
                        new PrimaryDrawerItem().withIdentifier(4).withName("Logout").withIcon(R.drawable.ic_settings_power_black_24dp)).
                withOnDrawerItemClickListener(this).
                build();

    }


    @Override
    public void onBackPressed() {
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Drawer item click listener
    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

        if (drawerItem instanceof Nameable) {

            switch (((Nameable) drawerItem).getName().getText(DashBoard.this)){
                case "Profile":
                    startActivity(new Intent(DashBoard.this,ProfileActivity.class));
                    break;
                case "Setting":
                    startActivity(new Intent(DashBoard.this,SettingActivity.class));
                    break;
                case "About":
                    startActivity(new Intent(DashBoard.this,AboutActivity.class));
                    break;
                case "Logout":
                   FireDataBaseHelper.mAuth.signOut();
                   startActivity(new Intent(DashBoard.this,SignInActivity.class));
                   finish();
                    break;
            }
           // Toast.makeText(DashBoard.this, ((Nameable) drawerItem).getName().getText(DashBoard.this)+"pos"+position, Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
