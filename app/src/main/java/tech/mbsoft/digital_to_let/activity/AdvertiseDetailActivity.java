package tech.mbsoft.digital_to_let.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.util.Util;

public class AdvertiseDetailActivity extends AppCompatActivity{

    TextView tvDetailRentType, tvDetailRoadName, tvDetailDistrict, tvDetailPoliceStation,
            tvDetailStartFrom, tvDetailDetail, tvDetailPrice, tvDetailName, tvDetailMobile, tvDetailPostOn;
    Button btMap;
    AdvertItem advertItem;
    FloatingActionButton fabEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_detail);
        Intent i = getIntent();
        advertItem= (AdvertItem) i.getSerializableExtra("ITEM");
        initUI();


    }

    private void initUI() {

        tvDetailRentType=findViewById(R.id.tvDetailRentType);
        tvDetailRoadName=findViewById(R.id.tvDetailRoadName);
        tvDetailDistrict=findViewById(R.id.tvDetailDistrict);
        tvDetailPoliceStation=findViewById(R.id.tvDetailPoliceStation);
        tvDetailStartFrom=findViewById(R.id.tvDetailStartFrom);
        tvDetailDetail=findViewById(R.id.tvDetailDetail);
        tvDetailPrice=findViewById(R.id.tvDetailPrice);
        tvDetailName=findViewById(R.id.tvDetailName);
        tvDetailMobile=findViewById(R.id.tvDetailMobile);
        tvDetailPostOn=findViewById(R.id.tvDetailPostOn);
        btMap=findViewById(R.id.btMap);
        fabEdit=findViewById(R.id.fabEdit);

        String dateStartFrom=""+advertItem.getStartFrom().getDate()+"-"+ Util.theMonth(advertItem.getStartFrom().getMonth())+"-"+advertItem.getStartFrom().getYear();
        String datePostOn=""+advertItem.getPost_on().getDate()+"-"+Util.theMonth(advertItem.getPost_on().getMonth())+"-"+advertItem.getPost_on().getYear();

        tvDetailRentType.setText(advertItem.getAdvertiseType());
        tvDetailRoadName.setText("Roadname: "+advertItem.getRoadName());
        tvDetailDistrict.setText("District: "+advertItem.getDistrict());
        tvDetailPoliceStation.setText("Police Stataion: "+advertItem.getPoliceStation());
        tvDetailStartFrom.setText("Rent from: "+dateStartFrom);
        tvDetailDetail.setText("Details: "+advertItem.getDetails());
        tvDetailPrice.setText("Price: "+advertItem.getPrice());
        tvDetailName.setText("Post by: "+advertItem.getPost_by_name());
        tvDetailMobile.setText("Mobile: "+advertItem.getMobile());
        tvDetailPostOn.setText("Post on: "+datePostOn);

        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdvertiseDetailActivity.this,EditAdvertiseItemDetailActivity.class);
                intent.putExtra("ITEM",advertItem);
                startActivityForResult(intent,100);
            }
        });

        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdvertiseDetailActivity.this,MapsActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK){
            AdvertItem advertItem=(AdvertItem) data.getSerializableExtra("EDIT_DONE");
            this.advertItem=advertItem;
            initUI();
            Log.e("Result ID",advertItem.getId());
        }
    }


}
