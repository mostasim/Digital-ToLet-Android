package tech.mbsoft.digital_to_let.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.internal.FirebaseAppHelper;

import java.util.Calendar;
import java.util.Date;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.model.User;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SignUpActivity";

    private FirebaseAuth mAuth;

    EditText etEmail,etPassword,etName;
    Button btSignUp,btSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialUI();
        mAuth = FirebaseAuth.getInstance();
    }

    private void initialUI() {
        etName=findViewById(R.id.etName);
        etEmail=findViewById(R.id.etSignUpEmail);
        etPassword=findViewById(R.id.etSignUpPassword);
        btSignIn=findViewById(R.id.btSignUp_SignIn);
        btSignUp=findViewById(R.id.btSignUp_SignUp);

        //ClickListener
        btSignUp.setOnClickListener(this);
        btSignIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btSignUp_SignUp:
                String name=etName.getText().toString();
                String email=etEmail.getText().toString();
                String password=etPassword.getText().toString();
                if (!email.isEmpty() && !password.isEmpty() &&!name.isEmpty())
                {
                    createUser(name,email,password);
                }else {
                    message("Fields are empty.");
                }
                break;
            case R.id.btSignUp_SignIn:
                    startActivity(new Intent(SignUpActivity.this,SignInActivity.class));
                break;

        }
    }
    private void createUser(final String name, final String email, final String password) {

        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            User user1=new User();
                            user1.setId(user.getUid());
                            user1.setName(name);
                            user1.setEmail(email);
                            user1.setRegistrationDate(new Date(Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DATE)));
                            user1.setPassword(password);
                            boolean isCreate=FireDataBaseHelper.createUserDetails(user1);
                            if (isCreate)
                                startActivity(new Intent(SignUpActivity.this,ProfileActivity.class));
                            else
                                message("User creation failed ...  ");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, "Sign up failed :(",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }
    private void message(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Mostasim: "+text);
    }
}
