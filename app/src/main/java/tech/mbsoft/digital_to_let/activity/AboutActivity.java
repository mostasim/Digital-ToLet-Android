package tech.mbsoft.digital_to_let.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tech.mbsoft.digital_to_let.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
