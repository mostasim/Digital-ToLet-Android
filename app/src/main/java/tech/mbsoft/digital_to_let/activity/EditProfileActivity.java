package tech.mbsoft.digital_to_let.activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.DataHelper;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.util.Util;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener,DatePickerDialog.OnDateSetListener{

    TextView tvEmail;
    EditText etName;
    Button btBirthDate,btEdit;

    DatePickerDialog datePickerDialog;

    DataHelper dataHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        dataHelper=DataHelper.getInstance();
        datePickerDialog = new DatePickerDialog(
                this, EditProfileActivity.this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE));

        initUI();
    }

    private void initUI() {

        etName=findViewById(R.id.etEditProfileName);
        tvEmail=findViewById(R.id.tvEditProfileEmail);

        btBirthDate=findViewById(R.id.btEditProfileBirthDate);
        btEdit=findViewById(R.id.btEditProfileEditSubmit);

        btBirthDate.setOnClickListener(this);
        btEdit.setOnClickListener(this);

        etName.setText(dataHelper.getUser().getName());
        tvEmail.setText(dataHelper.getUser().getEmail());

        if (dataHelper.getUser().getBirthDate()!=null)
        {
            btBirthDate.setText("Birth date: "+ Util.getDateFormate(dataHelper.getUser().getBirthDate()));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btEditProfileBirthDate:
                datePickerDialog.show();
                break;
            case R.id.btEditProfileEditSubmit:
                dataHelper.getUser().setName(etName.getText().toString());
                if (FireDataBaseHelper.createUserDetails(dataHelper.getUser())) {
                    setResult(RESULT_OK);
                    finish();
                }else
                    Toast.makeText(this, "Failed to edit profile...", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Date birthDate=new Date(year,month,dayOfMonth);
        dataHelper.getUser().setBirthDate(birthDate);
        btBirthDate.setText("Birth Date: "+Util.getDateFormate(birthDate));

    }
}
