package tech.mbsoft.digital_to_let.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.model.AdvertItem;

public class EditAdvertiseItemDetailActivity extends AppCompatActivity
        implements View.OnClickListener,DatePickerDialog.OnDateSetListener{

    private static final String TAG = "EditAdvertiseItemDetail";

    EditText etPostByName,etPrice,etDetails,etRoadName, etMobileNumber,etPoliceStation;
    Spinner spRentType,spSelectCity;
    Button btSelectFromDate,btSubmitAddPost;

    DatePickerDialog datePickerDialog;

    ArrayList<String> listRentType;
    ArrayList<String> listCity;

    Date startFrom,endFrom,currentDate;
    AdvertItem advertItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_advertise_item_detail);

        advertItem=(AdvertItem)getIntent().getSerializableExtra("ITEM");
        initUI();
        loadData();
        datePickerDialog = new DatePickerDialog(
                this, EditAdvertiseItemDetailActivity.this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE));
        loadRentCityData();

        ArrayAdapter<String> rentTypeAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listRentType);
        ArrayAdapter<String> cityAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listCity);

        rentTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spRentType.setAdapter(rentTypeAdapter);
        spSelectCity.setAdapter(cityAdapter);
    }

    private void loadData() {
        etPostByName.setText(advertItem.getPost_by_name());
        etPrice.setText(advertItem.getPrice());
        etDetails.setText(advertItem.getDetails());
        etRoadName.setText(advertItem.getRoadName());
        etMobileNumber.setText(advertItem.getMobile());
        etPoliceStation.setText(advertItem.getPoliceStation());
        startFrom=new Date(advertItem.getStartFrom().getDay(),advertItem.getStartFrom().getMonth(),advertItem.getStartFrom().getYear());
        currentDate=new Date(Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DATE));
    }


    private void initUI() {

        etPostByName=findViewById(R.id.etEditPostByName);
        etPrice=findViewById(R.id.etEditPrice);
        etDetails=findViewById(R.id.etEditDetails);
        etRoadName=findViewById(R.id.etEditRoadName);
        etMobileNumber =findViewById(R.id.etEditMobileNumber);
        etPoliceStation =findViewById(R.id.etEditPoliceStation);

        spRentType=findViewById(R.id.spEditSelectRentType);
        spSelectCity=findViewById(R.id.spEditSelectCity);

        btSelectFromDate=findViewById(R.id.btEditSelectFromDate);
        btSubmitAddPost=findViewById(R.id.btEditSubmitEditPost);

        btSelectFromDate.setOnClickListener(this);
        btSubmitAddPost.setOnClickListener(this);


    }
    private void loadRentCityData() {
        listRentType=new ArrayList<>();
        listCity=new ArrayList<>();

        listRentType.add("Family");
        listRentType.add("Mess");
        listRentType.add("Single Seat");
        listRentType.add("Room");
        listRentType.add("ATM");
        listRentType.add("Office");
        listRentType.add("Shop");

        listCity.add("Dhaka");
        listCity.add("Chittagong");
        listCity.add("Rangpur");
        listCity.add("Bogra");
        listCity.add("Dinajpur");
        listCity.add("Sylhet");
        listCity.add("Khulna");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btEditSubmitEditPost:
                AdvertItem postDetails=new AdvertItem();
                postDetails.setPost_by_name(etPostByName.getText().toString());
                postDetails.setAdvertiseType(listRentType.get(spRentType.getSelectedItemPosition()));
                postDetails.setDetails(etDetails.getText().toString());
                postDetails.setDistrict(listCity.get(spSelectCity.getSelectedItemPosition()));
                postDetails.setRoadName(etRoadName.getText().toString());
                postDetails.setPoliceStation(etPoliceStation.getText().toString());
                postDetails.setStartFrom(startFrom);
                postDetails.setPost_on(currentDate);
                postDetails.setMobile(etMobileNumber.getText().toString());
                postDetails.setPrice(etPrice.getText().toString());
                postDetails.setId(advertItem.getId()); //set id important
                if (FireDataBaseHelper.editAdvertiseItem(postDetails)) {
                    message("Advertise Edited");
                    Intent intent= new Intent();
                    intent.putExtra("EDIT_DONE",postDetails);
                    setResult(RESULT_OK,intent);
                    finish();
                }
                else
                    message("Some error occurred ...");

                break;
            case R.id.btEditSelectFromDate:
                datePickerDialog.show();
                break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        btSelectFromDate.setText(dayOfMonth+"/"+month+"/"+year);

        startFrom=new Date(year,month,dayOfMonth);
        currentDate=new Date(Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DATE));

    }
    private void message(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Mostasim: "+text);
    }
}
