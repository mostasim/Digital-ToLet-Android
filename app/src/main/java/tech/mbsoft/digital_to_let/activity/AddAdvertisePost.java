package tech.mbsoft.digital_to_let.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import tech.mbsoft.digital_to_let.R;
import tech.mbsoft.digital_to_let.database.FireDataBaseHelper;
import tech.mbsoft.digital_to_let.model.AdvertItem;
import tech.mbsoft.digital_to_let.util.Util;

public class AddAdvertisePost extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener ,LocationListener{

    private static final String TAG = "AddAdvertisePost";

    EditText etPostByName, etPrice, etDetails, etRoadName, etMobileNumber, etPoliceStation;
    Spinner spRentType, spSelectCity;
    Button btSelectFromDate, btSubmitAddPost;
    ToggleButton tbLocation;

    DatePickerDialog datePickerDialog;

    ArrayList<String> listRentType;
    ArrayList<String> listCity;
    tech.mbsoft.digital_to_let.model.Location locationltlng;
    Date startFrom, endFrom, currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertise_post);

        initUI();
        datePickerDialog = new DatePickerDialog(
                this, AddAdvertisePost.this, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE));
        loadRentCityData();

        ArrayAdapter<String> rentTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listRentType);
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listCity);

        rentTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spRentType.setAdapter(rentTypeAdapter);
        spSelectCity.setAdapter(cityAdapter);
    }

    private void getLocationData() {
        locationltlng=new tech.mbsoft.digital_to_let.model.Location();
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            message("Permission not access");
            return;
        }else
        {
            Location location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            //lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
            try {
               /* double longitude = location.getLongitude(); // TODO: 14-Apr-18 Null location handle
                double latitude = location.getLatitude();
                locationltlng.setLatitude(latitude);
                locationltlng.setLongitude(longitude);*/

               // message("Location "+" ," );
            }catch (Exception e)
            {
                message(e.toString());
            }

        }


    }

    private void loadRentCityData() {
        listRentType=new ArrayList<>();
        listCity=new ArrayList<>();

        listRentType.add("Family");
        listRentType.add("Mess");
        listRentType.add("Single Seat");
        listRentType.add("Room");
        listRentType.add("ATM");
        listRentType.add("Office");
        listRentType.add("Shop");

        listCity.add("Dhaka");
        listCity.add("Chittagong");
        listCity.add("Rangpur");
        listCity.add("Bogra");
        listCity.add("Dinajpur");
        listCity.add("Sylhet");
        listCity.add("Khulna");
    }

    private void initUI() {

        etPostByName=findViewById(R.id.etPostByName);
        etPrice=findViewById(R.id.etPrice);
        etDetails=findViewById(R.id.etDetails);
        etRoadName=findViewById(R.id.etRoadName);
        etMobileNumber =findViewById(R.id.etMobileNumber);
        etPoliceStation =findViewById(R.id.etPoliceStation);

        spRentType=findViewById(R.id.spSelectRentType);
        spSelectCity=findViewById(R.id.spSelectCity);

        btSelectFromDate=findViewById(R.id.btSelectFromDate);
        btSubmitAddPost=findViewById(R.id.btSubmitAddPost);

        tbLocation=findViewById(R.id.tbLocation);

        btSelectFromDate.setOnClickListener(this);
        btSubmitAddPost.setOnClickListener(this);
        tbLocation.setOnClickListener(this);


    }
    private void message(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Mostasim: "+text);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btSelectFromDate:
                datePickerDialog.show();
                break;
            case R.id.btSubmitAddPost:

                AdvertItem postDetails=new AdvertItem();
                postDetails.setPost_by_name(etPostByName.getText().toString());
                postDetails.setAdvertiseType(listRentType.get(spRentType.getSelectedItemPosition()));
                postDetails.setDetails(etDetails.getText().toString());
                postDetails.setDistrict(listCity.get(spSelectCity.getSelectedItemPosition()));
                postDetails.setRoadName(etRoadName.getText().toString());
                postDetails.setPoliceStation(etPoliceStation.getText().toString());
                postDetails.setStartFrom(startFrom);
                postDetails.setPost_on(currentDate);
                postDetails.setMobile(etMobileNumber.getText().toString());
                postDetails.setPrice(etPrice.getText().toString());

                if (FireDataBaseHelper.addNewAdvertise(postDetails))
                    message("Advertise Added");
                else
                    message("Some error occurred ...");
                break;
            case R.id.tbLocation:
                boolean on=tbLocation.isChecked();
                if (on)
                {
                    getLocationData();
                }else
                {
                    Toast.makeText(this, "Toogle off", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        btSelectFromDate.setText(dayOfMonth+" "+ Util.theMonth(month)+", "+year);

        startFrom=new Date(year,month,dayOfMonth);
        currentDate=new Date(Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DATE));

    }

    @Override
    public void onLocationChanged(Location location) {
            locationltlng.setLongitude(location.getLongitude());
            locationltlng.setLatitude(location.getLatitude());
            message("ChangedLocation: "+location.getLatitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
